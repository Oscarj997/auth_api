# Auth Api

## Instalar artisan 

php artisan install

## Generar llave

php artisan key:generate

## Ver .env.example

Observar el nombre de la base de datos

## Migrar bases de datos

php artisan migrate

### Ejecutamos las seeds

php artisan bd:seeds

## Generamos los token de passport

php artisan passport:install 

## iniciamos el servidor

php artisan run serve


