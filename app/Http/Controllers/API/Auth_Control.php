<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash ;
use Laravel\Passport\Bridge\AccessToken;

class Auth_Control extends Controller
{
    public function register(Request $request){ //Funcion para registar en la base de datos

        $validatedata = $request->validate([
            'name'=> 'required|max:255',
            'email' => 'required|email|unique:users',
            'password'=> 'required|confirmed'
        ]);
       $validatedata['password'] = Hash::make($request->password);

        $user=User::Create($validatedata);

        $accessToken=$user->createToken('AuthToken')->accessToken;


        return response([

            'user'=>$user, 
            'Access_Token'=> $accessToken

        ]);


    }

    public function Valition (Request $request){ //funcion para validar que el usuario este registrado
        $LoginData= $request-> validate([
            'email' => 'required|email|',
            'password'=> 'required'

        ]);
        $LoginData = request(['email', 'password']);

        if (!Auth::attempt($LoginData)){
            return(['error' => true]);
        }
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        return response(['user' => auth()->user(), 'access_Token' => $tokenResult->accessToken,'error'=>false]);
        
    }
    
    public function user(Request $request) 
    {//funcion para mostrar los datos de los usuarios

        $user=user::get();
        return response($user);
    }









}


