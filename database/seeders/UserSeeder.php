<?php

namespace Database\Seeders;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user= new User();
        $user->name='Oscar Jimenez';
        $user->email='Jimenezoscar997@gmail.com';
        $user->password=Hash::make('Password');

        $user->save();


        $user2= new User();
        $user2->name='Pedro Rodriguez';
        $user2->email='pedrorodriguez@gmail.com';
        $user2->password=Hash::make('Password2');
        $user2->save();

        $user3= new User();
        $user3->name='Edgar Marcano';
        $user3->email='EdgarMarcano@gmail.com';
        $user3->password=Hash::make('Password2');
        $user3->save();

        $user4= new User();
        $user4->name='Lupe Lopez';
        $user4->email='LupeLopez@gmail.com';
        $user4->password=Hash::make('Password3');
        $user4->save();
    }
}
